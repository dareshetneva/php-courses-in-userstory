<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 18.10.18
 * Time: 16:57
 */

// убрать br
// обычный массив с числами
$array = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
// обычный массив со строками
$lang = array("PHP", "RUBY", "HTML", "CSS");
// ассоциативный массив
$words = array('drink' => 'tea', 'dessert' => 'cupcake', 'meat' => 'pork');
// вывод массива
print_r($array);
print_r($lang);
print_r($words);
// вывод конкретного элемента в массиве
print_r($array[5]);
echo PHP_EOL;
print_r($words['dessert']);
echo PHP_EOL;
// количество элементов в массиве
echo count($array);
echo PHP_EOL;
echo count($lang);
echo PHP_EOL;
echo count($words);
echo PHP_EOL;
// поиск элемента в массиве, выведет ключ
echo array_search('pork', $words);
echo PHP_EOL;
if (in_array("PHP", $lang))
{
    echo "Нашел PHP";
    echo PHP_EOL;
}
// реверсирование массива
$array = array_reverse($array);
print_r($array);
// перемешивание элементов массива
// при применении к ассоциативноу массиву, ключи поменяется на стандартные
shuffle($array);
print_r($array);
// сортировка элементов массива по возврастанию
asort($array);
print_r($array);
// сортировка элементов массива по убыванию
arsort($array);
print_r($array);
// сортировка ключей массива по возврастанию
ksort($array);
print_r($array);
// сортировка ключей массива по убыванию
krsort($array);
print_r($array);
// вывод последнего элемента массива
echo end($lang);
echo PHP_EOL;
// добавление элементов в конец массива
array_push($lang, "PHP", "CSS", "PHP");
print_r($lang);
$countAll = array_count_values($lang);
print_r($countAll);
// объединение элементов массива в строку
$url = implode('/', $words);
echo $url;
echo PHP_EOL;
// разделение строки на массив
$str = "Предложение с нескольким количеством пробелов";
$from_str = explode(' ', $str);
print_r($from_str);
// применяет функцию ко всем элементам массива
function plusOne($num)
{
    $num += 1;
    return $num;
}
$array = array_map('plusOne', $array);
print_r($array);
// слияние двух массивов
$new_array = array_merge($lang, $words);
print_r($new_array);