<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 19.10.18
 * Time: 18:01
 */

function generate_email($num1, $num2, $num3)
{
    echo "Numbers for generating: $num1 $num2 $num3\n";

    $result = "";
    for ($i = 0; $i < $num1; $i++)
        $result = $result.chr(random_int(97,122));

    $result = $result.'@';

    for ($i = 0; $i < $num2; $i++)
        $result = $result.chr(random_int(97,122));

    $result = $result.'.';

    for ($i = 0; $i < $num3; $i++)
        $result = $result.chr(random_int(97,122));

    echo $result."\n";
}

generate_email(7, 5, 3);
generate_email(8, 4, 4);
generate_email(13, 5, 2);