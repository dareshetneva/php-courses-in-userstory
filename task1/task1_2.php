<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 19.10.18
 * Time: 9:34
 */
$example = '    Some words in some sentence in some program as example    ';
// вывод строки в консоль
echo $example;
echo PHP_EOL;
// удаляет пробелы (или другие символы) из начала и конца строки
$example = trim($example);
echo $example;
echo PHP_EOL;
// число символов в строке
echo strlen($example);
echo PHP_EOL;
// вывод символа по номеру в строке
echo $example[8];
echo PHP_EOL;
// перевод первой буквы каждого слова в верхний регистр
echo ucwords($example);
echo PHP_EOL;
// перевод в верхний регистр
echo strtoupper($example);
echo PHP_EOL;
// перевод в нижний регистр
echo strtolower($example);
echo PHP_EOL;
// перевод первой буквы предложения в верхний регистр
echo ucfirst($example);
echo PHP_EOL;
// вернет true если в строке есть такая подстрока
if (strstr($example, 'some'))
    echo 'Есть подстрока some';
else echo 'Нет подстроки some';
echo PHP_EOL;
// вернет позицию первого вхождения подстроки в строку
echo strpos($example, 'some');
echo PHP_EOL;
// вне зависимости от регистра
echo stripos($example, 'some');
echo PHP_EOL;
// вернет позицию последнего вхождения подстроки в строку
echo strrpos($example, 'some');
echo PHP_EOL;
// вне зависимости от регистра
echo strripos($example, 'some');
echo PHP_EOL;
// разбиение на массив, в каждой ячейке которого хранится одинаковое количество (или меньше) символов из строки
$words = str_split($example, 5);
print_r($words);
// выводит строку начиная с первого вхождения любого из указанных символов
echo strpbrk($example, 'iht');
echo PHP_EOL;
// выделяет подстроку
echo substr($example, 5, 15);
echo PHP_EOL;
// выводит количество вхождений подстроки
echo substr_count($example, 'some');
echo PHP_EOL;
// заменяет все вхождения подстроки
echo str_replace('some','other', $example);
echo PHP_EOL;
// реверс строки
echo strrev($example);
echo PHP_EOL;
// случайным образом меняет порядок символов
echo str_shuffle($example);
echo PHP_EOL;
// заполнение строки до определенной длины
echo str_pad($example, 100, "*", STR_PAD_BOTH);

