<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 20.10.18
 * Time: 14:05
 */

/**
 * Класс Vehicle
 *
 * Класс связан логически с классом Autosalon (с помощью свойства) и Application (с помощью массива). Другими словами,
 * машина знает в каком автосалоне продается, и какие заявки на нее поступили.
 */
class Vehicle
{

    /**
     * @var string имя марки машины
     */
    private $brandName;
    /**
     * @var integer вместимость салона машины
     */
    private $maxPassengersCount;
    /**
     * @var integer стоимость
     */
    private $cost;
    /**
     * @var integer количество машин этой марки на складе
     */
    private $inStorageCount;
    /**
     * @var bool наличие машины в салоне
     */
    private $isInSalon;

    /**
     * @var string название салона, где выставляется данная марка
     */
    private $autosalon;
    /**
     * @var array заявки на машину данной марки
     */
    private $applications = array();

    /**
     * Метод get для переменной brandName
     *
     * Метод, позволяющий получить значение закрытой переменой brandName вне класса Vehicle
     *
     * @return string
     */
    public function getBrandName()
    {
        return $this->brandName;
    }

    /**
     * Метод set для переменной brandName
     *
     * Метод, позволяющий установить значение закрытой переменой brandName вне класса Vehicle
     *
     * @param string $newBrandName
     */
    public function setBrandName($newBrandName)
    {
        $this->brandName = $newBrandName;
    }

    /**
     * Метод get для переменной maxPassengersCount
     *
     * Метод, позволяющий получить значение закрытой переменой maxPassengersCount вне класса Vehicle
     *
     * @return integer
     */
    public function getMaxPassengersCount()
    {
        return $this->maxPassengersCount;
    }

    /**
     * Метод set для переменной maxPassengersCount
     *
     * Метод, позволяющий установить значение закрытой переменой maxPassengersCount вне класса Vehicle
     *
     * @param integer $newMaxPassengersCount
     */
    public function setMaxPassengersCount($newMaxPassengersCount)
    {
        $this->maxPassengersCount = $newMaxPassengersCount;
    }

    /**
     * Метод get для переменной cost
     *
     * Метод, позволяющий получить значение закрытой переменой cost вне класса Vehicle
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Метод set для переменной cost
     *
     * Метод, позволяющий установить значение закрытой переменой cost вне класса Vehicle
     *
     * @param integer $newCost
     */
    public function setCost($newCost)
    {
        $this->cost = $newCost;
    }

    /**
     * Метод get для переменной inStorageCount
     *
     * Метод, позволяющий получить значение закрытой переменой inStorageCount вне класса Vehicle
     *
     * @return integer
     */
    public function getInStorageCount()
    {
        return $this->inStorageCount;
    }

    /**
     * Метод set для переменной inStorageCount
     *
     * Метод, позволяющий установить значение закрытой переменой inStorageCount вне класса Vehicle
     *
     * @param integer $newInStorageCount
     */
    public function setInStorageCount($newInStorageCount)
    {
        $this->inStorageCount = $newInStorageCount;
    }

    /**
     * Метод get для переменной isInSalon
     *
     * Метод, позволяющий получить значение закрытой переменой isInSalon вне класса Vehicle
     *
     * @return bool
     */
    public function getIsInSalon()
    {
        return $this->isInSalon;
    }

    /**
     * Метод set для переменной isInSalon
     *
     * Метод, позволяющий установить значение закрытой переменой isInSalon вне класса Vehicle
     *
     * @param  bool $isInSalon
     */
    public function setIsInSalon($isInSalon)
    {
        $this->isInSalon = $isInSalon;
    }

    /**
     * Метод get для переменной autosalon
     *
     * Метод, позволяющий получить значение закрытой переменой autosalon вне класса Vehicle
     *
     * @return string
     */
    public function getAutosalon()
    {
        return $this->autosalon;
    }

    /**
     * Метод set для переменной autosalon
     *
     * Метод, позволяющий установить значение закрытой переменой autosalon вне класса Vehicle
     *
     * @param string $autosalon
     */
    public function setAutosalon($autosalon)
    {
        $this->autosalon = $autosalon;
    }

    /**
     * Метод get для переменной applications
     *
     * Метод, позволяющий получить значение закрытой переменой applications вне класса Vehicle
     *
     * @return array
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Метод добавления заявки на машину.
     *
     * Метод, позволяющий добавить в массив заявок новый элемент. При добавлении элемента изменяется его свойство:
     * устанавливается имя конкретной марки машины, на которую подается заявка. Элемент добавляется в конец массива.
     *
     * @param Application $newAppl
     */
    public function addApplication(Application $newAppl)
    {
        $newAppl->setCarName($this->getBrandName());
        array_push($this->applications,$newAppl);
    }

    /**
     * Метод удаления заявки на машину.
     *
     * Метод, позволяющий удалить заявку на магину из массива. В массиве ищется конкретная заявка, если находится, то
     * объект заявки разрушается, если нет, то выдается сообщение об ошибке.
     *
     * @param Application $invalidAppl
     */
    public function removeApplication(Application $invalidAppl)
    {
        if (($key = array_search($invalidAppl, $this->applications)) !== false)
            unset($this->applications[$key]);
        else echo "Не найден объект заявки. Проверьте введенные данные.\n";
    }
}