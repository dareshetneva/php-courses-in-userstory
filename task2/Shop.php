<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 23.10.18
 * Time: 23:58
 */

/**
 * Класс Shop
 *
 * Класс является родительским для класса Autosalon и содержит в себе базовые методы и поля, характеризующие любой магазин
 *
 * @package task2
 */
class Shop {

    /**
     * @var string имя магазина
     */
    private $name = '';

    /**
     * Метод get для переменной name
     *
     * Метод, позволяющий получить значение закрытой переменой name вне класса Shop
     *
     * @return string имя магазина
     */
    public function getName()
    {
        return $this -> name;
    }

    /**
     * Метод set для переменной name
     *
     * Метод, позволяющий установить значение закрытой переменой name вне класса Shop
     *
     * @param string $newName новое значение имени магазина
     */
    public function setName($newName)
    {
        $this -> name = $newName;
    }
}
