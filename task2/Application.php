<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 20.10.18
 * Time: 14:18

*/

/**
 * Класс Application
 *
 * Класс связан логически с классом Vehicle. Моделирует объект заявки на машину.
 */
class Application {

    /**
     * @var string имя заказчика
     */
    private $customerName;
    /**
     * @var string телефон заказчика
     */
    private $telephone;
    /**
     * @var string название марки машины, на которую подается заявка
     */
    private $carName;

    /**
     * Метод get для переменной customerName
     *
     * Метод, позволяющий получить значение закрытой переменой customerName вне класса Application
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Метод set для переменной customerName
     *
     * Метод, позволяющий установить значение закрытой переменой customerName вне класса Application
     *
     * @param string $newCustomerName
     */
    public function setCustomerName($newCustomerName)
    {
        $this->customerName = $newCustomerName;
    }

    /**
     * Метод get для переменной telephone
     *
     * Метод, позволяющий получить значение закрытой переменой telephone вне класса Application
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Метод set для переменной telephone
     *
     * Метод, позволяющий установить значение закрытой переменой telephone вне класса Application
     *
     * @param mixed $newTelephone
     */
    public function setTelephone($newTelephone)
    {
        $this->telephone = $newTelephone;
    }

    /**
     * Метод get для переменной carName
     *
     * Метод, позволяющий получить значение закрытой переменой carName вне класса Application
     *
     * @return string
     */
    public function getCarName()
    {
        return $this->carName;
    }

    /**
     * Метод set для переменной carName
     *
     * Метод, позволяющий установить значение закрытой переменой carName вне класса Application
     *
     * @param string $carName
     */
    public function setCarName($carName)
    {
        $this->carName = $carName;
    }
}