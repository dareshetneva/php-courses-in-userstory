<?php
/**
 * Created by PhpStorm.
 * User: darya
 * Date: 20.10.18
 * Time: 14:21
 */

require 'Application.php';
require 'AutoSalon.php';
require 'Vehicle.php';

// создаем объекты различных классов
$myAutoSalon = new AutoSalon();
$myAutoSalon->setName('LuxuryAuto');

$bentley = new Vehicle();
$bentley->setBrandName('Bentley Motors');
$bentley->setCost('5000000');
$bentley->setMaxPassengersCount(4);
$bentley->setInStorageCount(0);
// изначально машина не в салоне
$bentley->setisInSalon(false);

$porsche = new Vehicle();
$porsche->setBrandName('Porsche');
$porsche->setCost('6000000');
$porsche->setMaxPassengersCount(2);
$porsche->setInStorageCount(1);
// изначально машина не в салоне
$porsche->setisInSalon(false);

$appl1 = new Application();
$appl1->setCustomerName('Anastasya Solovyeva');
$appl1->setTelephone('89517538264');

$appl2 = new Application();
$appl2->setCustomerName('Natalya Vershinina');
$appl2->setTelephone('89517748264');

$appl3 = new Application();
$appl3->setCustomerName('Alena Ivanova');
$appl3->setTelephone('89537748244');

$appl4 = new Application();
$appl4->setCustomerName('Svetlana Romanova');
$appl4->setTelephone('89537148244');

print_r($myAutoSalon);
print_r($bentley);
print_r($porsche);
print_r($appl1);
print_r($appl2);
print_r($appl3);
print_r($appl4);

// добавляем машины в автосалон
$myAutoSalon->addVehicle($bentley);
$myAutoSalon->addVehicle($porsche);

// назначаем заявки
$bentley->addApplication($appl1);
$bentley->addApplication($appl3);
$porsche->addApplication($appl2);
$porsche->addApplication($appl4);

print_r($bentley->getApplications());
print_r($porsche->getApplications());

// обрабатываем заявки
$myAutoSalon->checkApplications($bentley);
$myAutoSalon->checkApplications($porsche);
